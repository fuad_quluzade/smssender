package az.lsim.smssender.request;

import lombok.Data;

import java.util.Date;

@Data
public class ReqSms {
//    private Long smsId;
    private String msisdn;
    private String senderName;
    private String messageBody;
    private Date doneDate;
}
