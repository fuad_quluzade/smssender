package az.lsim.smssender.exception;

public class ExceptionConstants {

    public static final Integer INTERNAL_EXCEPTION=500;

    public static final Integer INVALID_MSISDN=100;
}
