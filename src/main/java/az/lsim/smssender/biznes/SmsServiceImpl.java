package az.lsim.smssender.biznes;

import az.lsim.smssender.exception.ExceptionConstants;
import az.lsim.smssender.model.dto.RespStatus;
import az.lsim.smssender.model.dto.RespStatusList;
import az.lsim.smssender.model.entity.Sms;
import az.lsim.smssender.request.ReqSms;
import az.lsim.smssender.resopitory.SmsRepository;
import az.lsim.smssender.schedule.MySchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class SmsServiceImpl implements SmsService{

    Sms sms=new Sms();

    @Autowired
    private SmsRepository smsRepository;

    @Override
    public boolean addSms(ReqSms reqSms) {
        boolean result=false;
        RespStatusList response = new RespStatusList();
        try {

            String msisdn = reqSms.getMsisdn();
            String senderName = reqSms.getSenderName();
            String messageBody = reqSms.getMessageBody();

            if(!checkNumber(msisdn)
            || msisdn.equals(sms.getMsisdn())){
                response.setStatus(new RespStatus(ExceptionConstants.INVALID_MSISDN, "INVALID MSISDN"));
            }else {
                Sms sms = new Sms();
                sms.setMsisdn(msisdn);
                sms.setSenderName(senderName);
                sms.setMessageBody(messageBody);
                smsRepository.addSms(sms);
                response.setStatus(RespStatus.getSuccessMessage());
                result=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "INTERNAL EXCEPTION"));
        }
    return result;
    }








    public static boolean checkNumber(String input){
        boolean result =false;
          String [] arr={"55","50","77","70","99","51"};
          for (int i=0;i<arr.length;i++){
              if(input.startsWith("994") && input.contains(arr[i]) && input.length()==12){
                  result=true;
              }
          }
         return result;
        }
    }

