package az.lsim.smssender.biznes;

import az.lsim.smssender.model.dto.RespStatusList;

public interface ClientService {
    RespStatusList getSmsByRequest(String msisdn, String senderName, String messageBody);
}
