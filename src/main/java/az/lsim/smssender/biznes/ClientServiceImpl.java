package az.lsim.smssender.biznes;

import az.lsim.smssender.model.dto.RespStatus;
import az.lsim.smssender.model.dto.RespStatusList;
import az.lsim.smssender.model.entity.Sms;
import az.lsim.smssender.resopitory.SmsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService{

    @Autowired
    private SmsRepository smsRepository;

    @Override
    public RespStatusList getSmsByRequest(String msisdn, String senderName, String messageBody) {
        RespStatusList response=new RespStatusList();
        if(msisdn !=null &&
                senderName != null &&
                messageBody != null) {
//            List<Sms> unsendmessage=mySchedule.check();
            Sms sms = smsRepository.getSmsByRequest(msisdn, senderName, messageBody);

            Optional<Sms> sms1 = Optional.of(sms);
            if (sms1.isPresent()) {
//                smsRepository.updateSmsByRequest(sms);
                response.setStatus(RespStatus.getSuccessMessage());
            } else {
                response.setStatus(RespStatus.getErrorMessage());
            }
        }
        return response;
    }

}
