package az.lsim.smssender.model.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Sms {

    private Long id;
    private String msisdn;
    private String senderName;
    private String messageBody;
    private Date doneDate;

}
