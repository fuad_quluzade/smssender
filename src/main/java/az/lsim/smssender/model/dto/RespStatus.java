package az.lsim.smssender.model.dto;

//@Data
//@NoArgsConstructor
//@AllArgsConstructor
public class RespStatus {
    private  int  statusCode;
    private String statusMessage;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }



    public RespStatus(int statusCode, String statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    private static final Integer SUCCESS_CODE=0;
    private static final String  SUCCESS_MESSAGE="Success";

    public static RespStatus getSuccessMessage(){
        return new RespStatus(SUCCESS_CODE,SUCCESS_MESSAGE);

    }

    private static final Integer ERROR_CODE=1;
    private static final String  ERROR_MESSAGE="Error";

    public static RespStatus getErrorMessage(){
        return new RespStatus(ERROR_CODE,ERROR_MESSAGE);

    }
}
