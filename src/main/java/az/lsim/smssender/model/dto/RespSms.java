package az.lsim.smssender.model.dto;

import lombok.Data;

import java.util.Date;

@Data
public class RespSms {
    private Long SmsId;
    private String msisdn;
    private String senderName;
    private String messageBody;
    private Date doneDate;
}
