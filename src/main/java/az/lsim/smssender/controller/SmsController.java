package az.lsim.smssender.controller;

import az.lsim.smssender.biznes.SmsService;
import az.lsim.smssender.model.dto.RespStatusList;
import az.lsim.smssender.request.ReqSms;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class SmsController {

    @Autowired
    SmsService smsService;

    @PostMapping(value = "/addSms")
    public ResponseEntity<?> addSms(@RequestBody ReqSms reqSms){
      boolean result =  smsService.addSms(reqSms);
      if(result)
        return ResponseEntity.status(201).build();
      else
          return ResponseEntity.status(500).build();
    }



//    @GetMapping("/getSmsByRequest")
//    public RespStatusList getTestByName(@RequestParam String msisdn,
//                                        @RequestParam String senderName,
//                                        @RequestParam String messageBody){
//        return smsService.getSmsByRequest(msisdn,senderName,messageBody);
//    }


}
