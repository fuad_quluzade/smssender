package az.lsim.smssender.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DbHelper {
    @Value("${api.datasource.url}")
    private String url;

    @Value("${api.datasource.username}")
    private String username;

    @Value("${api.datasource.password}")
    private String password;

    @Value("${api.datasource.driver-class-name}")
    private String driverClassName;

//    @ConfigurationProperties(prefix = "api.datasource")
    @Bean(name ="dataSource")
    public DataSource getConnection(){
        return DataSourceBuilder.create()
                .driverClassName(driverClassName)
                .url(url)
                .username(username)
                .password(password).build();
    }
}
