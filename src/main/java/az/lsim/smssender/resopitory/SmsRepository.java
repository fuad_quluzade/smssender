package az.lsim.smssender.resopitory;

import az.lsim.smssender.model.entity.Sms;

import java.util.List;

public interface SmsRepository {
    void addSms(Sms sms) throws Exception;

    Sms getSmsByRequest(String msisdn,String senderName,String messageBody) ;

    void updateSmsByRequest(Sms sms);

    List<Sms> checkDb();
}
