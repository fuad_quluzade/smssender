package az.lsim.smssender.resopitory;

import az.lsim.smssender.model.entity.Sms;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class SmsRepositoryImpl implements SmsRepository{

    @Autowired
    private DataSource dataSource;

    @Override
    public void addSms(Sms sms) throws Exception{
        JdbcTemplate jdbcTemplate= new JdbcTemplate(dataSource);
        String sql="insert into sms(id,msisdn,sender_Name,message_Body) " +
                " values(sms_seq.nextval,?,?,?) ";
        jdbcTemplate.update(sql,new Object[]{sms.getMsisdn(),sms.getSenderName(),sms.getMessageBody()});
    }


    @Override
    public Sms getSmsByRequest(String msisdn, String senderName, String messageBody) {
        JdbcTemplate jdbcTemplate= new JdbcTemplate(dataSource);
        String sql="select * from sms where msisdn=? and sender_name=? and message_body=? ";
        return (Sms) jdbcTemplate.queryForObject(sql,new Object[]{msisdn,senderName,messageBody},new BeanPropertyRowMapper(Sms.class));
    }

    @Override
    public void updateSmsByRequest(Sms sms) {
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        String sql="update sms set is_sent=1 , done_date=sysdate  where msisdn=? and sender_Name=? and message_Body=? ";
        jdbcTemplate.update(sql,new Object[]{sms.getMsisdn(),sms.getSenderName(),sms.getMessageBody()});
    }

    @Override
    public List<Sms> checkDb() {
        JdbcTemplate jdbcTemplate= new JdbcTemplate(dataSource);
        String sql="select * from sms where is_sent=0 and insert_date>=sysdate-5 ";
        List<Sms> check=jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(Sms.class));
        return  check;
    }

}
