package az.lsim.smssender.schedule;

import az.lsim.smssender.model.entity.Sms;
import az.lsim.smssender.resopitory.SmsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MySchedule {

    @Autowired
    SmsRepository smsRepository;

    @Scheduled(fixedDelay = 1000)
    public List<Sms> check(){
        List<Sms> smsList=smsRepository.checkDb();
        return smsList;
    }
}
